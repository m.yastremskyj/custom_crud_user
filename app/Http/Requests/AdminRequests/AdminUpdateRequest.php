<?php

namespace App\Http\Requests\AdminRequests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class AdminUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (auth()->check() && auth()->user()->is_admin == 1);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => "required|string|max:50|unique:users,name,{$this->user->id}",
            'email' => "required|email|max:255|unique:users,email,{$this->user->id}",
//            'name' => [
//                'required',
//                'string',
//                'max:50',
//                Rule::unique('users', 'name')->ignore($this->user)
//            ],
//            'email' => [
//                'required',
//                'email',
//                'max:255',
//                Rule::unique('users', 'email')->ignore($this->user)
//            ],
        ];
    }
}
