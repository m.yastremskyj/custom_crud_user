<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CheckIfAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if (!auth()->user()) {
            return redirect('dashboard')->withErrors(__('Login Before'));
        }
        else if (!$this->checkIfUserIsAdmin(auth()->user())) {
            redirect('dashboard')->withErrors(__('Access is for adults only! Get out the little bastard!'));
        } else {
            return $next($request);
        }
        return $next($request);
    }

    private function checkIfUserIsAdmin($user)
    {
        return ($user->is_admin == 1);
    }

}
