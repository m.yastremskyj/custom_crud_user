<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\AdminRequests\AdminStoreRequest;
use App\Http\Requests\AdminRequests\AdminUpdateRequest;
use App\Models\User;
use Illuminate\Http\Request;

class AdminDashboardController extends Controller
{
    public function index() {
        $users = User::latest()->paginate(10);

        return view('dashboard.admin.dashboard', compact('users'));
    }

    public function create()
    {
        return view('dashboard.admin.create');
    }

    public function store(User $user, AdminStoreRequest $request)
    {
        $user->create($request->validated());

        return redirect()->route('admin.dashboard')
            ->withSuccess(__('User created successfully.'));
    }

    public function show(User $user)
    {
        return view('dashboard.admin.show', [
            'user' => $user
        ]);
    }

    public function edit(User $user)
    {
        return view('dashboard.admin.edit', [
            'user' => $user
        ]);
    }

    public function update(User $user, AdminUpdateRequest $request)
    {
        $user->update($request->validated());

        return redirect()->route('admin.dashboard')
            ->withSuccess(__('User updated successfully.'));
    }

    public function destroy(User $user)
    {
        if ($user->id === auth()->user()->id) {
            return redirect()->route('admin.dashboard')
                ->withErrors(__('You are trying to delete yourself.'));
        }
        $user->delete();

        return redirect()->route('admin.dashboard')
            ->withSuccess(__('User deleted successfully.'));
    }

}
