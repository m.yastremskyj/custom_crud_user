<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\AdminRequests\AdminUpdateRequest;
use App\Http\Requests\UserRequests\UserUpdateRequest;
use App\Models\User;
use Illuminate\Http\Request;

class UserDashboardController extends DashboardController
{
    public function index()
    {
        $user = User::where('id', auth()->user()->id)->firstOrFail();
        return view('dashboard.users.dashboard', compact('user'));
    }

    public function edit()
    {
        $user = User::where('id', auth()->user()->id)->firstOrFail();
        return view('dashboard.users.edit', [
            'user' => $user
        ]);
    }

    public function update(User $user, UserUpdateRequest $request)
    {
        $user->update($request->validated());

        return redirect()->route('user.dashboard')
            ->withSuccess(__('User updated successfully.'));
    }
}
