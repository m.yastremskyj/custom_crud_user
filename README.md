## Requirements: ``php^8.1``, ``node 16.15.1``, ``npm 8.11.0``

## SSH Project-cloning

> git clone git@gitlab.com:m.yastremskyj/custom_crud_user.git

## HTTPS Project-cloning

> git clone https://gitlab.com/m.yastremskyj/custom_crud_user.git

```bash
cp .env.example .env
composer install
php artisan key:generate
php artisan storage:link
php artisan migrate --seed
npm install
npm run dev
```

## The so-called admin panel: accesses

Login  | Password
------------- | -------------
admin@admin.com  | password

## User panel: accesses

> Watch your database after seeding
