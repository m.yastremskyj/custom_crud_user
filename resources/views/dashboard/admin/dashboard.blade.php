@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Dashboard') }}</div>

                    <div class="card-body">
                        @if (session('success-login'))
                            <div class="alert alert-success" role="alert">
                                {{ session('success-login') }}
                            </div>
                        @elseif(session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        {{ __('You are logged in! P.S. Sobaka-ulybaka') }}
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="card">
            <div class="card-body">
                <div class="bg-light p-4 rounded">
                    @if (auth()->user()->is_admin === 1)
                        <h1>Users</h1>
                        <div class="lead">
                            Manage your users here.
                            <a href="{{ route('admin.create') }}" class="btn btn-primary btn-sm float-right">Add new user</a>
                        </div>
                    @endif

                    <div class="mt-2">
                        @include('partials.messages')
                    </div>
                    @if (auth()->user()->is_admin === 1)
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th scope="col" width="1%">#</th>
                                    <th scope="col" width="15%">Name</th>
                                    <th scope="col">Email</th>
                                    <th scope="col" width="10%">Username</th>
                                    <th scope="col" width="1%" colspan="3"></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($users as $user)
                                    <tr>
                                        <th scope="row">{{ $user->id }}</th>
                                        <td>{{ $user->name }}</td>
                                        <td>{{ $user->email }}</td>
                                        <td>{{ $user->username }}</td>
                                        <td><a href="{{ route('admin.show', $user->id) }}" class="btn btn-warning btn-sm">Show</a></td>
                                        <td><a href="{{ route('admin.edit', $user->id) }}" class="btn btn-info btn-sm">Edit</a></td>
                                        <td>
                                            {!! Form::open(['method' => 'DELETE','route' => ['admin.destroy', $user->id],'style'=>'display:inline']) !!}
                                            {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-sm']) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                            <div class="d-flex">
                                {!! $users->links() !!}
                            </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
