@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-body">
                <div class="bg-light p-4 rounded">
                    <h1>Show user</h1>
                    <div class="lead">

                    </div>

                    <div class="container mt-4">
                        <div>
                            Name: {{ $user->name }}
                        </div>
                        <div>
                            Email: {{ $user->email }}
                        </div>
                    </div>

                </div>
                <div class="mt-4">
                    <a href="{{ route('admin.edit', $user->id) }}" class="btn btn-info">Edit</a>
                    <a href="{{ route('admin.dashboard') }}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
