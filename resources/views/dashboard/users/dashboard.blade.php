@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Dashboard') }}</div>

                    <div class="card-body">
                        @if (session('success-login'))
                            <div class="alert alert-success" role="alert">
                                {{ session('success-login') }}
                            </div>
                        @elseif(session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        {{ __('You are logged in! P.S. Sobaka-podozrevaka') }}
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="card">
            <div class="card-body">
                <div class="bg-light p-4 rounded">
                    <div class="mt-2">
                        @include('partials.messages')
                    </div>

                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th scope="col" width="1%">#</th>
                            <th scope="col" width="15%">Name</th>
                            <th scope="col">Email</th>
                            <th scope="col" width="10%">Username</th>
                            <th scope="col" width="1%" colspan="3"></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <th scope="row">{{ $user->id }}</th>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->username }}</td>
                            <td><a href="{{ route('user.edit', $user->id) }}" class="btn btn-info btn-sm">Edit</a></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
