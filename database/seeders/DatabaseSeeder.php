<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::factory(10)->create();

        \App\Models\User::factory()->create([
            'name' => 'Adminoff',
            'email' => 'admin@admin.com',
            'email_verified_at' => now(),
            'is_admin' => 1,
            'password' => Hash::make('password'),
            'remember_token' => Str::random(10),
        ]);

    }
}
