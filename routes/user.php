<?php

use App\Http\Controllers\Dashboard\UserDashboardController;
use Illuminate\Support\Facades\Route;


Route::controller(UserDashboardController::class)->group(function () {
    Route::get('/dashboard', 'index')->name('user.dashboard');
    Route::get('/{user}/edit', 'edit')->name('user.edit');
    Route::patch('/{user}/update', 'update')->name('user.update');
});
