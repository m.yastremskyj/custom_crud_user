<?php

use App\Http\Controllers\Dashboard\AdminDashboardController;
use Illuminate\Support\Facades\Route;

Route::group([
    'prefix' => 'admin',
    'middleware' => 'is_admin',
], function () {
    Route::controller(AdminDashboardController::class)->group(function () {
        Route::get('/dashboard', 'index')->name('admin.dashboard');
        Route::get('/create', 'create')->name('admin.create');
        Route::post('/create', 'store')->name('admin.store');
        Route::get('/{user}/show', 'show')->name('admin.show');
        Route::get('/{user}/edit', 'edit')->name('admin.edit');
        Route::patch('/{user}/update', 'update')->name('admin.update');
        Route::delete('/{user}/delete', 'destroy')->name('admin.destroy');
    });
});
